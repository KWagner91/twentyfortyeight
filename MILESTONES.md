# MILESTONES of 2048

## AIMS
A functional version of 2048, the game is played with user keyboard arrow keys. The game uses a 3x3 2D Array, the stone positions are displayed to the user.

## MILESTONES TO REACH
1) Set up project structure (model, Input Handler, Timer, Graphics)
2) model with Class Stone and Class Board (start with 1 Stone on field)
3) Set up Input Handler and Timer for 1 Stone
4) Graphics and Canvas :scream: :fire:
5) Game is functional with 1 Stone moving left, right, up, down accross the game field :fire:
6) Introduce a second Stone after user Input
7) Two stones move accross the game field and don't overlap
8) Implement logic for stones to merge if same number
9) Game is fully functional
10) Victory output is implemented
11) Basic game design and restart button is implemented

## BONUS
* Highscore
* Modus with pictures instead of numbers
* Implement a success bar while user is playing (based on points)