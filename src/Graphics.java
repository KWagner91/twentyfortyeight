import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import model.Model;

public class Graphics {
    private Model model;
    private GraphicsContext gc;


    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        // i = row, j = column (4 x 4)
        // window size = 800 x 800 -> 1 square 200 x 200
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (model.board[i][j] != null) {
                    gc.fillRect( 200 * i,  200 * j, model.HEIGHT / 4, model.WIDTH / 4);
                    gc.setFill(Color.YELLOW);
                    gc.setFont(new Font(150));
                    gc.setTextAlign(TextAlignment.CENTER);
                    gc.setTextBaseline(VPos.CENTER);
                    gc.setStroke(Color.BLACK);
                    gc.strokeText("2", 200 * i + model.WIDTH/8,
                            200 * j + model.HEIGHT/8);
                }
            }

        }
    }
        }








