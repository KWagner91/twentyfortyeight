import javafx.animation.AnimationTimer;
import model.Model;


public class Timer extends AnimationTimer {

    private Graphics graphics;
    public Model model;

    public Timer(Model model, Graphics graphics) {
        this.graphics = graphics;
        this.model = model;
    }

    @Override
    public void handle(long nowNano) {
        graphics.draw();
    }
}
