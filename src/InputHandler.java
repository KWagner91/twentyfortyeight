import javafx.scene.input.KeyCode;
import model.Model;
import model.Stone;


public class InputHandler {
    public Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPress(KeyCode keycode) {
        if (keycode == KeyCode.UP) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (model.board[i][j] != null) {
                        model.board[i][j - 1] = model.board[i][j];
                        model.board[i][j] = null;
                        break;
                    }
                }
            }
            model.addStone();
        }

        if (keycode == KeyCode.DOWN) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (model.board[i][j] != null) {
                        model.board[i][j + 1] = model.board[i][j];
                        model.board[i][j] = null;
                        break;
                    }
                }
            }
            model.addStone();
        }

        if (keycode == KeyCode.LEFT) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (model.board[i][j] != null) {
                        model.board[i - 1][j] = model.board[i][j];
                        model.board[i][j] = null;
                        break;
                    }
                }
            }
            model.addStone();
        }

        if (keycode == KeyCode.RIGHT) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (model.board[i][j] != null) {
                        model.board[i + 1][j] = model.board[i][j];
                        model.board[i][j] = null;
                        break;
                    }
                }
            }
            model.addStone();
        }

    }
}



